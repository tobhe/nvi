Source: nvi
Section: editors
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Standards-Version: 4.6.2
Build-Depends:
 debhelper-compat (= 13),
 libncurses-dev,
 libdb-dev
Rules-Requires-Root: no
Homepage: https://repo.or.cz/nvi.git
Vcs-Git: https://salsa.debian.org/debian/nvi.git
Vcs-Browser: https://salsa.debian.org/debian/nvi

Package: nvi
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: nvi-doc
Conflicts: nex
Description: 4.4BSD re-implementation of vi
 Vi is the original screen based text editor for Unix systems.
 It is considered the standard text editor, and is available on
 almost all Unix systems.
 .
 Nvi is intended as a "bug-for-bug compatible" clone of the original
 BSD vi editor. As such, it doesn't have a lot of snazzy features as do
 some of the other vi clones such as elvis and vim. However, if all
 you want is vi, this is the one to get.

Package: nvi-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}
Description: 4.4BSD re-implementation of vi - documentation files
 Vi is the original screen based text editor for Unix systems.
 It is considered the standard text editor, and is available on
 almost all Unix systems.
 .
 Nvi is intended as a "bug-for-bug compatible" clone of the original
 BSD vi editor. As such, it doesn't have a lot of snazzy features as do
 some of the other vi clones such as elvis and vim. However, if all
 you want is vi, this is the one to get.
 .
 This package contains the tutorials and the reference manual.
